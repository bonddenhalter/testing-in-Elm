# Testing in Elm

* Add tests to Tests.elm
* Be sure to import any modules needed
* Run `elm-test` in Terminal to run the tests
* Here is a quick reference of tests you can run using Expect.
    * `equal` (arg2 == arg1)
    * `notEqual` (arg2 /= arg1)
    * `lessThan` (arg2 < arg1)
    * `atMost` (arg2 <= arg1)
    * `greaterThan` (arg2 > arg1)
    * `atLeast` (arg2 >= arg1)
    * `true` (arg == True)
    * `false` (arg == False)


Here is the reference for Expect in the elm-test package: 

http://package.elm-lang.org/packages/elm-community/elm-test/2.1.0/Expect
* There are examples of how to use these tests, as well as some additional tests.
* Also check out Fuzzers.